<?php

/**
 * @file
 * Plugin to provide an relationship handler for a child term from a term.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Child term from term'),
  'description' => t('Creates a terms context from a term.'),
  'context' => 'ctools_child_term_relationship_child_term_from_term',
  'keyword' => 'child_term',
  'description' => t('Adds a taxonomy term child from a term context.'),
  'required context' => new ctools_context_required(t('Term'), 'entity:taxonomy_term'),
  'edit form' => 'ctools_child_term_relationship_child_term_settings_form',
  'defaults' => array('delta' => 0),
);

/**
 * Return a new context based on an existing context.
 */
function ctools_child_term_relationship_child_term_from_term($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('term');
  }

  if (isset($context->data)) {
    $tid = $context->data->tid;
    if ($tid && !isset($children[$tid])) {
      $query = db_select('taxonomy_term_data', 't');
      $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
      $query->addField('t', 'tid');
      $query->condition('h.parent', $tid);
      $query->addTag('term_access');
      $query->orderBy('t.weight');
      $query->orderBy('t.name');
      $query->range($conf['delta'], 1);
      $child_tid = $query->execute()->fetchField();
      if (!empty($child_tid)) {
        $term = taxonomy_term_load($child_tid);
        return ctools_context_create('entity:taxonomy_term', $term);
      }
    }
  }
}

/**
 * Settings form for the argument
 */
function ctools_child_term_relationship_child_term_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];
  
  $form['delta'] = array(
    '#title' => t('Delta'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(0, 100)),
    '#default_value' => $conf['delta'],
    '#description' => t('Child terms will be sorted by weight and name, and then the one with the specified delta will be returned.'),
  );
  
  return $form;
}
